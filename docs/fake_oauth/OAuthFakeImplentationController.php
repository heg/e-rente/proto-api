<?php

namespace Dibs\Api\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;
use Predis\ClientInterface as RedisClient;

class OAuthFakeImplentationController
{
    public function auth(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();
        $code = '1234';

        return $twig->render($response, 'auth.twig', [
        'redirect_uri' => $query['redirect_uri'],
        'state'        => $query['state'],
        'code'         => $code,
        ]);
    }

    public function token(Response $response)
    {
        $oauth = [
        "token_type"    => "Bearer",
        "access_token"  => "1234",
        "refresh_token" => "1234",
        "expires"       => time() + (3600 * 24)
        ];
        $response->getBody()->write(json_encode($oauth));

        return $response
        ->withHeader('Content-Type', 'application/json');
    }

    public function grant(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();

        return $twig->render($response, 'grant.twig', [
        'redirect_uri' => $query['redirect_uri'],
        ]);
    }

    public function grantIsOk(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();
        $this->redisClient->set('fake_grant_is_ok', true, 'EX', 3600);

        return $response
        ->withHeader('Location', $query['redirect_uri'])
        ->withStatus(302);
    }
}
