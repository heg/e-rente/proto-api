# Implementation OpenPK

Le but de l'implémentation d'OpenPK de votre côté est de pouvoir fournir un tableau de données de ce type :

```
    [
      "id" => "votre_caisse_de_pension",
      "name" => "Le nom de la caisse de pension",
      "nameDe" => "Le nom de la caisse de pension",
      "address" => "Votre adresse",
      "openpkStatus" => "active",
      "openpkServices" => [
        "authorizeUrl" => "ENDPOINT_OAUTH/auth",
        "tokenEndpoint" => "ENDPOINT_OAUTH/token",
        "grantUrl" => "ENDPOINT_OAUTH/grant",
        "identityProvider" => "N'EST PAS UTILE DANS UN PREMIER TEMPS",
        "policyEndpoint" => "ENDPOINT_API_OPENPK_POLICY",
        "consentEndpoint" => "ENDPOINT_API_OPENPK_CONSENT"
      ]
    ];
```

avec ces informations :

- `ENDPOINT_OAUTH` : le point d'entrée vers votre serveur oAuth. Le workflow oAuth2 que nous suivont est le [Authorization Code](https://oauth.net/2/grant-types/authorization-code/)
  + __Important__ : les urls du workflow oauth n'ont pas besoin d'etre `ENDPOINT_OAUTH/auth`, `ENDPOINT_OAUTH/token`, `ENDPOINT_OAUTH/grant`, êlles peuvent être d'une forme différente, sur différents domaines et avec différentes fin d'url ... mais les trois doivent être présentes 
- `ENDPOINT_API_OPENPK_POLICY` : le point d'entrée vers votre implémentation des [apis consents](https://api-spec-dot-acrea-openpk.appspot.com/consent.html#tag/consent) 
- `ENDPOINT_API_OPENPK_CONSENT` : le point d'entrée vers votre implémentation des [apis policy](https://api-spec-dot-acrea-openpk.appspot.com/policies.html#tag/policy) 

## Créer un "faux" serveur oAuth répondant au grant type `authorization code` :

Si dans la phase de prototypage vous devez d'abord créer un faux serveur oAuth, c'est possible.
Voici les pré-requis :

1. Pouvoir répondre aux appels d'api et retourner les bonnes informations lors des redirections
2. Faire faire à l'utilisateur un chemin oauth crédible

## Exemple de "faux" serveur oAuth

Les exemples de codes sont en pseudo-code/PHP.

Nous avons un controlleur [OAuthFakeImplentationController](fake_oauth/OAuthFakeImplentationController.php),
voici les urls qui lui sont associés :

```
GET  : /protocol/openid-connect/auth     => OAuthFakeImplentationController::auth
POST : /protocol/openid-connect/token    => OAuthFakeImplentationController::token
GET  : /protocol/openid-connect/grant    => OAuthFakeImplentationController::grant
GET  : /protocol/openid-connect/grant/ok => OAuthFakeImplentationController::grantIsOk
```

### GET : /protocol/openid-connect/auth

Cet appel est celui qui sera appelé lorsque l'utilisateur va devoir se connecter à la caisse de pension. Le controlleur va devoir présenter un formulaire de connexion.

#### Fonctionnement de cette étape oAuth

L'utilisateur va fournir dans la requêtes deux paramètres important: `redirect_uri`, `state`.
Une fois la fausse connexion faite, votre api va devoir rediriger l'utilisateur sur ce `redirect_uri` en passant en ajoutant en paramètre le même `state` que celui fourni et y ajouter un `code` qui peut avoir n'importe quelle valeur.

#### Exemple d'implémentation :

La fonction [OAuthFakeImplentationController::auth](fake_oauth/OAuthFakeImplentationController.php?ref_type=heads#L12) récupère les paramètres passées par l'utilisateur, crée un `code` factice et va ensuite afficher [un formulaire de connexion](fake_oauth/auth.twig). Celui-ci, une fois validé, va rediriger l'utilisateur vers la `redirect_url` en passant le `state` et le `code` en paramètre.

ex: 

```
<form action="{{redirect_uri}}" method="GET">
    <label for="username">Nom d'utilisateur :</label>
    <!-- <input type="text" id="username" name="username" required> -->
    <input type="text" id="username" name="username">

    <label for="password">Mot de passe :</label>
    <!-- <input type="password" id="password" name="password" required> -->
    <input type="password" id="password" name="password">
    <input type="hidden" name="state" value="{{state}}">
    <input type="hidden" name="code" value="{{code}}">

    <button type="submit">Se connecter</button>
</form>
```

#### __IMPORTANT__ : Pour différencier les cas d'utlisateur

Si jamais vous voulez différencier les cas d'utilisateur, il ne faut pas fournir à tout le monde le même `code`. Car c'est le `code` qui va permettre de différencier les utilisateurs. Une technique pourrait être de remplacer dans le formulaire ci-dessus le champs `username` par `code`, ce qui permettra dans l'appel suivant de savoir quel est l'utilisateur intervenant.

### POST : /protocol/openid-connect/token

Cet appel est celui qui fournira à l'utilisateur les tokens de connexions et qui permettra donc d'identifier l'utilisateur lors des requêtes.

#### Fonctionnement de cette étape oAuth

L'utilisateur fourni dans le corps de la requête le `code` à vérifier. Une fois ce code vérifié, le serveur oauth lui retourne un ensemble de token de la forme suivante :

```
 [
  "token_type"    => "Bearer",
  "access_token"  => "1234",
  "refresh_token" => "1234",
  "expires"       => time() + (3600 * 24)
]
```

#### Exemple d'implémentation :

La fonction [OAuthFakeImplentationController::token](fake_oauth/OAuthFakeImplentationController.php?ref_type=heads#L24) ne vérifie rien et retourne automatiquement un access_token valable pendant 24h.

#### __IMPORTANT__ : Pour différencier les cas d'utlisateur

Si jamais vous voulez différencier les cas d'utilisateur, c'est ici qu'il faut lire le `code` fourni et renvoyer des tokens permettant de distinguer les différents appels. La solution la plus simple serait de retourner le `code` en `access_token`. Ce `code` pouvant être le nom d'utilisateur (voir étape précedante haut), c'est une technique simple pour différencier les utilisateurs lors des prochaines requêtes.

### GET : /protocol/openid-connect/grant => OAuthFakeImplentationController::grant
### GET : /protocol/openid-connect/grant/ok => OAuthFakeImplentationController::grantIsOk

Si vous avez besoin d'implémenter un formulaire ou l'utilisateur doit consentir au partage de données entre votre caisse et OpenPK, c'est à l'appel `/grant` qu'il faut répondre.

#### Fonctionnement de cette étape oAuth

Cet appel ne sera pas systématiquement utilisé. Suite à l'étape précédente l'utilisateur obitent un `access_token` permettant d'appeler l'API OpenPK implémentée par votre caisse. L'API va donc essayer d'appeler l'endpoint [Get granted consent](https://api-spec-dot-acrea-openpk.appspot.com/consent.html#tag/consent/operation/getUsersMe) pour voir si elle accès à des contrats de pension (`policyId`). Si le tableau de `policyGrants` est vide, il va donc falloir que l'utilisateur consente au partage de données entre votre caisse et e-rentes.ch.

L'utilisateur va être redirigé vers votre fomulaire de consentement. 
Dans la requête d'affichage de formulaire est fourni les trois paramètres suivants : `redirect_uri`, `pension_id`, `client_id`, `pension_id`.
Une fois le processus de consentement fini, l'utlisateur devra être redirigé vers la `redirect_uri`.

#### Exemple d'implémentation :

Les fonctions [OAuthFakeImplentationController::grant](fake_oauth/OAuthFakeImplentationController.php?ref_type=heads#L38) et [OAuthFakeImplentationController::grantIsOk](fake_oauth/OAuthFakeImplentationController.php?ref_type=heads#L47) implémente une solution possible (mais non pérenne du tout) d'implémentation. Le prérequis de cet implémentation et qu'il n'y a qu'un seul utilisateur, ce qui n'est pas du tout le cas sur le long terme donc la solution la plus simple et optimale n'a pas encore été trouvé par l'auteur.

Ce que fait l'implémentation et la chose suivante :

- L'utilisateur est redirigé sur l'url de la caisse `/grant` dans la fonction [OAuthFakeImplentationController::grant](fake_oauth/OAuthFakeImplentationController.php?ref_type=heads#L38)
- On présente [un formulaire de consentement](fake_oauth/grant.twig) à l'utilisateur. 
- Quand l'utilisateur l'accepte, il est redirigé vers l'url de la caisse `/grant/ok`. 
- Dans cet appel on enregistre (ici via redis, mais d'autres solutions seraient souhaitables) ce consentement.
- Lors des appels à l'api [Get granted consent](https://api-spec-dot-acrea-openpk.appspot.com/consent.html#tag/consent/operation/getUsersMe), on vérifie si le consentement à été donné et en fonction de ça on retourne, ou non, les données des contrats de pension.

#### __IMPORTANT__ : Pour différencier les cas d'utlisateur

L'implémentation donnée ne permet pas de différencier les cas utilisateur. L'auteur n'a pas, pour le moment, trouvé de solution toute faite mais réfléchi activement à ce sujet :-) 
Si jamais vous aviez trouvé une solution simple : je suis preneur !

N'hésitez pas à me contacter à autour de ce sujet : joel.israel@hesge.ch