<?php

namespace Dibs\Api\Open3K;

use Dibs\Api\User\UserManager;

class Open3KManager
{
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function getInfo()
    {
        $open3k =  $this->userManager->retrieveFromPrivateDb('open3k');
        $info = [
            'solde'   => $open3k['solde'],
            'amount'  => $open3k['amount'],
            'rate'    => $open3k['rate'],
            'paydate' => $open3k['paydate'],
            'payout_predication' => $open3k['payout_predication'],
        ];

        return $info;
    }

    public function reset()
    {
        $this->saveInfo(null, null, null, null);
    }

    public function saveInfo($solde, $amount, $rate, $paydate, $payoutPredication)
    {
        $this->userManager->appendToPrivateDb('open3k.solde', $solde);
        $this->userManager->appendToPrivateDb('open3k.amount', $amount);
        $this->userManager->appendToPrivateDb('open3k.rate', $rate);
        $this->userManager->appendToPrivateDb('open3k.paydate', $paydate);
        $this->userManager->appendToPrivateDb('open3k.payout_predication', $payoutPredication);
    }

    public function savePaydate($paydate)
    {
        return $this->userManager->appendToPrivateDb('open3k.paydate', $paydate);
    }

    public function getPaydate()
    {
        return $this->userManager->retrieveFromPrivateDb('open3k.paydate');
    }
}
