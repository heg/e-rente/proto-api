<?php

namespace Dibs\Api\OpenPV;

use Dibs\Api\Data\DataManager;
use Dibs\Api\Exceptions\UnauthorizedException;
use Dibs\Api\OpenPV\OAuthProviderFactory;
use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\User\UserManager;
use GuzzleHttp\ClientInterface as HttpClient;
use League\OAuth2\Client\Token\AccessToken;

class OpenPVManager
{
    protected $httpClient;
    protected $dataManager;
    protected $userManager;
    protected $oAuthProviderFactory;

    public function __construct(HttpClient $httpClient, DataManager $dataManager, UserManager $userManager, OAuthProviderFactory $oAuthProviderFactory)
    {
        $this->httpClient           = $httpClient;
        $this->dataManager          = $dataManager;
        $this->userManager          = $userManager;
        $this->oAuthProviderFactory = $oAuthProviderFactory;
    }

    public function reset()
    {
        $this->saveSelection(null);
    }

    public function saveSelection($ids)
    {
        $this->userManager->appendToPrivateDb('openpv.pensions.selection', $ids);
        $this->userManager->appendToPrivateDb('openpv.providents', null);
    }

    public function getSelection()
    {
        return $this->userManager->retrieveFromPrivateDb('openpv.pensions.selection');
    }

    public function getGrants()
    {
        $grants = $this->userManager->retrieveFromPrivateDb('openpv.pensions.grants');
        return $grants ? json_decode($grants, true) : [];
    }

    public function getTokens()
    {
        $tokens = $this->userManager->retrieveFromPrivateDb('openpv.pensions.consents');
        return $tokens ? json_decode($tokens, true) : [];
    }

    public function getUserData()
    {
        return $this->userManager->retrieveFromPrivateDb('openpv.pensions') ?? [];
    }

    public function getRegistry($filtered = false)
    {
        $registry = $this->dataManager->get('openpv::registry');

        // if (!$registry) {
        $response = $this->httpClient->request('GET', 'registry');
        $registry = (string) $response->getBody();

        $this->dataManager->set('openpv::registry', $registry, 60 * 30);
        // }

        $registry = json_decode($registry);

        if ($filtered) {
            $registry = array_map(
                function ($pension) {
                    unset($pension->services);
                    unset($pension->status);
                    unset($pension->address);

                    return $pension;
                },
                (array) $registry
            );
        }

        $registry = array_combine(array_column($registry, 'id'), $registry);
        return $registry;
    }

    public function getNextConsentRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getSelection();
        $tokens    = $this->getTokens();

        $toBeConsented = array_filter(
            $selection,
            function ($id) use ($tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testTokenIsValid($info, $token);
            }
        );

        return sizeof($toBeConsented) > 0 ? $registry[array_shift($toBeConsented)] : null;
    }

    public function getNextGrantRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getSelection();
        $grants    = $this->getGrants();
        $tokens    = $this->getTokens();

        $toBeGranted = array_filter(
            $selection,
            function ($id) use ($grants, $tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testGrantIsValid($info, $token);
            }
        );

        return sizeof($toBeGranted) > 0 ? $registry[array_shift($toBeGranted)] : null;
    }

    public function getGrantUrl($registryInfo)
    {
        $oAuthProvider    = $this->getOAuthProvider($registryInfo);
        $authorizationUrl = $oAuthProvider->getGrantUrl($registryInfo);

        return $authorizationUrl;
    }

    public function getAuthorizationUrl($registryInfo)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);

        $authorizationUrl                 = $oAuthProvider->getAuthorizationUrl(['scope' => 'offline_access openid profile email']);
        $_SESSION['openPVOauth2state']    = $oAuthProvider->getState();
        $_SESSION['openPVOauth2pkceCode'] = $oAuthProvider->getPkceCode();

        return $authorizationUrl;
    }

    public function isStateOkay(string $state = null)
    {
        return $state
        && isset($_SESSION['openPVOauth2state'])
            && ($state == $_SESSION['openPVOauth2state']);
    }

    public function getProvidents(SimulationData $simulationData = null)
    {
        $providents = false && $this->userManager->retrieveFromPrivateDb('openpv.providents');

        if ($simulationData || !$providents) {
            $registry   = $this->getRegistry();
            $providents = array_reduce(
                $this->getSelection() ?? [],
                function ($providents, $entityId) use ($registry, $simulationData) {

                    if (!isset($registry[$entityId])) {
                        return $providents;
                    }

                    $entityInfo  = $registry[$entityId];
                    $accessToken = $this->getValidAccessToken($entityInfo);

                    $oAuthProvider = $this->getOAuthProvider($entityInfo);
                    $request       = $oAuthProvider->getAuthenticatedRequest('GET', $entityInfo->services->consentEndpoint . '/users/me', $accessToken);
                    $response      = $oAuthProvider->send($request);
                    $grant         = json_decode($response->getBody()->getContents());

                    if ($simulationData) {
                        $request = $oAuthProvider->getAuthenticatedRequest(
                            'POST',
                            $entityInfo->services->providentEndpoint . '/providents/' . $grant->providentGrants[0]->providentId . '/salary-change-simulation',
                            $accessToken,
                            [
                                'body'    => json_encode($simulationData),
                                'headers' => [
                                    'Content-Type' => 'application/json',
                                ],
                            ]
                        );
                    } else {
                        $url = $entityInfo->services->providentEndpoint . '/providents/' . $grant->providentGrants[0]->providentId;

                        if (isset($entityInfo->services->providentQuery)) {
                            $url = $entityInfo->services->providentQuery;
                            $url = str_replace('[CONTRACT]', $grant->providentGrants[0]->providentId, $url);
                        }

                        $request = $oAuthProvider->getAuthenticatedRequest('GET', $url, $accessToken);
                    }

                    $response = $oAuthProvider->send($request);

                    $provident = json_decode($response->getBody()->getContents());

                    if (is_array($provident)) {
                        $solde = 0;
                        $payoutpredication = 0;

                        foreach ($provident as $p) {
                            if (isset($p->solde)) {
                                $solde += $p->solde;
                            }
                            if (isset($p->payoutpredication)) {
                                $payoutpredication += $p->payoutpredication;
                            }
                        }

                        if ($payoutpredication > 0) {
                            $provident = [
                              'payout_predication' => $payoutpredication,
                              'solde' => $solde,
                            ];
                        }
                    }

                    $providents[$entityId] = $provident;

                    return $providents;
                },
                []
            );

            if (!$simulationData) {
                $this->userManager->appendToPrivateDb('openpv.providents', json_encode($providents));
            }
        } else {
            $providents = json_decode($providents, true);
        }

        return $providents;
    }

    protected function getValidAccessToken($registryInfo)
    {
        $tokens      = $this->getTokens();
        $providentId = $registryInfo->id;

        if (!isset($tokens[$providentId])) {
            throw new UnauthorizedException("Les consentements de la pension " . $providentId . " n'ont pas encore été donnés");
        }

        $accessToken = $this->testTokenIsValid($registryInfo, $tokens[$providentId]);

        if (!$accessToken) {
            unset($tokens[$providentId]);
            $this->userManager->appendToPrivateDb('openpv.pensions.consents', json_encode($tokens));
            throw new UnauthorizedException("Les tokens de la pension " . $providentId . " ne sont plus valides");
        }

        return $accessToken;
    }

    protected function testTokenIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        return $accessToken;
    }

    protected function testGrantIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        $oAuthProvider = $this->getOAuthProvider($registryInfo);
        $resourceOwner = $oAuthProvider->getResourceOwner($accessToken);
        $resourceOwner = $resourceOwner->toArray();

        return $resourceOwner['personGrant'] ?? false;
    }

    protected function getOAuthProvider($registryInfo)
    {
        $openPVInfo = $registryInfo->services;
        return $this->oAuthProviderFactory->getOAuthProvider($openPVInfo);
    }

    protected function refreshAccessToken($registryInfo, AccessToken $accessToken)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);

        try {
            $newAccessToken = $oAuthProvider->getAccessToken(
                'refresh_token',
                [
                    'refresh_token' => $accessToken->getRefreshToken(),
                ]
            );
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de rafraichir le token: ' . $e->getMessage());
        }

        $this->storeAccessToken($registryInfo, $newAccessToken);

        return $newAccessToken;
    }

    public function retrieveAccessToken($registryInfo, string $code)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);
        $oAuthProvider->setPkceCode($_SESSION['openPVOauth2pkceCode']);
        $accessToken = $oAuthProvider->getAccessToken(
            'authorization_code',
            [
                'code' => $code,
            ]
        );

        $this->storeAccessToken($registryInfo, $accessToken);
    }

    protected function storeAccessToken($registryInfo, AccessToken $accessToken)
    {
        $tokens                    = $this->getTokens();
        $tokens[$registryInfo->id] = $accessToken;
        $this->userManager->appendToPrivateDb('openpv.pensions.consents', json_encode($tokens));
        $this->userManager->appendToPrivateDb('openpv.providents', null);
    }
}
