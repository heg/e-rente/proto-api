<?php

namespace Dibs\Api\OpenPV;

use Dibs\Api\OpenPV\OAuthProvider;

class OAuthProviderFactory
{
    protected $clientId;
    protected $clientSecret;
    protected $redirectUri;
    protected $redirectGrantUri;
    protected $timeout;

    public function __construct($clientId, $clientSecret, $redirectUri, $redirectGrantUri, $timeout)
    {
        $this->clientId         = $clientId;
        $this->clientSecret     = $clientSecret;
        $this->redirectUri      = $redirectUri;
        $this->redirectGrantUri = $redirectGrantUri;
        $this->timeout          = $timeout;
    }

    public function getOAuthProvider($openPVRegistryInfo)
    {
        return new OAuthProvider(
            [
            'pkceMethod'              => \League\OAuth2\Client\Provider\GenericProvider::PKCE_METHOD_S256,
            'timeout'                 => $this->timeout,
            'clientId'                => $this->clientId,
            'clientSecret'            => $this->clientSecret,
            'redirectUri'             => $this->redirectUri,
            'redirectGrantUri'        => $this->redirectGrantUri,
            'urlAuthorize'            => $openPVRegistryInfo->authorizeUrl,
            'urlAccessToken'          => $openPVRegistryInfo->tokenEndpoint,
            'urlResourceOwnerDetails' => $openPVRegistryInfo->consentEndpoint . '/users/me',
            ]
        );
    }
}
