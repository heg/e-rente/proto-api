<?php

namespace Dibs\Api\State;

use Dibs\Api\OpenPK\OpenPKManager;

class StateManager
{
    protected $openPKManager;

    public function __construct(OpenPKManager $openPKManager)
    {
        $this->openPKManager = $openPKManager;
    }

    public function getCurrent()
    {
        $states = [];

        // states liés à OpenPK
        $data = $this->openPKManager->getPensionFundUserData();

        // Est-ce que l'on a séléctionné les caisses de pensions ?
        $states['pension_funds_selected'] = $data['selection'] && (sizeof($data['selection']) > 0);

        // Est-ce que l'on a récupéré les consentements liés aux caisses de pensions ?
        $states['pension_funds_consents'] = $data['consents'] && (sizeof($data['consents']) > 0);

        return $states;
    }
}
