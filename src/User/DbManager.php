<?php

namespace Dibs\Api\User;

use Dibs\Api\Data\DataManager;
use Dibs\Api\Exceptions\PathNotFoundInDbException;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class DbManager
{
    protected $dataManager;
    protected $resourceOwner;

    const DB_MODEL = [
        'version' => 6,
        'openak'  => [
            'navs'       => null,
            'birthDate'  => null,
            'relationships' => null,
            'sex' => null,
            'calculator' => null,
        ],
        'openpk'  => [
            'pensions' => [
                'selection' => null,
                'consents'  => null,
                'grants'    => null,
            ],
            'policies' => null,
        ],
        'openpv'  => [
            'pensions' => [
                'selection' => null,
                'consents'  => null,
                'grants'    => null,
            ],
            'providents' => null,
        ],
        'open3k'  => [
            'solde'   => null,
            'amount'  => null,
            'rate'    => null,
            'paydate' => null,
            'payout_predication' => null,
        ],
    ];

    public function __construct(DataManager $dataManager)
    {
        $this->dataManager = $dataManager;
    }

    public function reset()
    {
        $result = $this->saveDb(
            $this->getKeyHash(),
            self::DB_MODEL
        );
    }

    /**
     * Initialise la base de donnée (vérifie si il y a une migration à faire)
     *
     * @param ResourceOwnerInterface $resourceOwner propriétaire de labse
     */
    public function init(ResourceOwnerInterface $resourceOwner)
    {
        $this->resourceOwner = $resourceOwner;

        $key = $this->getKeyHash();
        $db  = $this->getDb($key);

        if (!$db) {
            $db = self::DB_MODEL;
            $this->saveDb($key, $db);
        }

        $this->checkIntegrity($key);
    }

    /**
     * Enregistre une valeur dans la base
     *
     * @param string  $path chemin sous la forme folder.subfolder.subsubfolder ...
     * @param <mixed> $data données à enregistrer
     */
    public function set(string $path, $data)
    {
        $key = $this->getKeyHash();
        $db  = $this->getDb($key);

        $paths   = explode('.', $path);
        $pointer = &$db;

        foreach ($paths as $subPath) {
            if (!array_key_exists($subPath, $pointer)) {
                throw new PathNotFoundInDbException("Tentative d'écriture du chemin inexistant '$path'");
            }

            $pointer = &$pointer[$subPath];
        }

        $pointer = $data;

        $this->saveDb($key, $db);
    }

    /**
     * Récupère une donnée de la base
     *
     * @param  string $path chemin sous la forme folder.subfolder.subsubfolder ...
     * @return <mixed> Valeur en base
     */
    public function get(string $path)
    {
        $key = $this->getKeyHash();
        $db  = $this->getDb($key);

        $paths   = explode('.', $path);
        $pointer = $db;

        foreach ($paths as $subPath) {
            if (!array_key_exists($subPath, $pointer)) {
                throw new PathNotFoundInDbException("Tentative de lecture du chemin inexistant '$path'");
            }

            $pointer = $pointer[$subPath];
        }

        return $pointer;
    }

    protected function getKeyHash()
    {
        $publicKey = $this->getPublicKey();
        $id        = $this->resourceOwner->getId();

        $key = hash(
            $_ENV['HASH_ALGO'],
            $id . ':' . $_ENV['SECRET_KEY'] . ':' . $publicKey
        );

        return $key;
    }

    protected function getPublicKey()
    {
        $dataArray = $this->resourceOwner->toArray();

        return $dataArray['email'];
    }

    protected function getDb($key)
    {
        $this->dataManager->touch($key, $_ENV['DB_TTL']);

        $db = $this->dataManager->get($key);
        $db = $this->decrypt($db, $this->getPublicKey());

        return json_decode($db, true);
    }

    protected function checkIntegrity($key)
    {
        $db = $this->getDb($key);

        if ($db['version'] != self::DB_MODEL['version']) {
            $db = $this->migrateDb($db);
            $this->saveDb($key, $db);
        }
    }

    protected function migrateDb($db)
    {
        throw new \Exception("La migration de DB n'a pas encore été implémentée", 500);
    }

    protected function saveDb($key, array $db)
    {
        $dataArray = $this->resourceOwner->toArray();
        $db        = json_encode($db);
        $encrypt   = $this->encrypt($db, $this->getPublicKey());

        $this->dataManager->set($key, $encrypt, $_ENV['DB_TTL']);
    }

    private function encrypt($plaintext, $password, $encoding = null)
    {
        $password = $password . ':' . $_ENV['SECRET_KEY'];

        $keysalt         = openssl_random_pseudo_bytes(16);
        $key             = hash_pbkdf2("sha512", $password, $keysalt, 20000, 32, true);
        $iv              = openssl_random_pseudo_bytes(openssl_cipher_iv_length("aes-256-gcm"));
        $tag             = "";
        $encryptedstring = openssl_encrypt($plaintext, "aes-256-gcm", $key, OPENSSL_RAW_DATA, $iv, $tag, "", 16);

        return $encoding == "hex" ? bin2hex($keysalt . $iv . $encryptedstring . $tag) : ($encoding == "base64" ? base64_encode($keysalt . $iv . $encryptedstring . $tag) : $keysalt . $iv . $encryptedstring . $tag);
    }

    private function decrypt($encryptedstring, $password, $encoding = null)
    {
        $password = $password . ':' . $_ENV['SECRET_KEY'];

        $encryptedstring = $encoding == "hex" ? hex2bin($encryptedstring) : ($encoding == "base64" ? base64_decode($encryptedstring) : $encryptedstring);
        $keysalt         = substr($encryptedstring, 0, 16);
        $key             = hash_pbkdf2("sha512", $password, $keysalt, 20000, 32, true);
        $ivlength        = openssl_cipher_iv_length("aes-256-gcm");
        $iv              = substr($encryptedstring, 16, $ivlength);
        $tag             = substr($encryptedstring, -16);

        return openssl_decrypt(substr($encryptedstring, 16 + $ivlength, -16), "aes-256-gcm", $key, OPENSSL_RAW_DATA, $iv, $tag);
    }
}
