<?php

namespace Dibs\Api\User;

use Dibs\Api\Login\LoginManager;
use Dibs\Api\User\DbManager;

class UserManager
{
    public function __construct(protected DbManager $dbManager, protected LoginManager $loginManager)
    {
        try {
            $this->dbManager->init($this->loginManager->getResourceOwner());
        } catch (\Exception $e) {
        }
    }

    /**
     * Reset les valeurs dans la base utilisateur
     */
    public function reset()
    {
        $this->dbManager->reset();

        foreach ($_COOKIE as $cookie) {
            $parts = explode('=', $cookie);
            $name  = trim($parts[0]);
            setcookie($name, '', time() - 1000);
            setcookie($name, '', time() - 1000, '/');
        }
    }

    /**
     * Ajoute des valeurs dans la base utilisateur
     *
     * @param string  $path chemin du type folder.subfolder.subsubfolder ...
     * @param <mixed> $data données à enregistrer
     */
    public function appendToPrivateDb($path, $data)
    {
        $this->dbManager->set($path, $data);
    }

    /**
     * Récupère des données de la base utilisateur
     *
     * @param  string  $path   chemin du type folder.subfolder.subsubfolder ...
     * @return <mixed> données dans la base
     */
    public function retrieveFromPrivateDb($path)
    {
        return $this->dbManager->get($path);
    }
}
