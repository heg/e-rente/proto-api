<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\OpenAK\OpenAKManager;
use Dibs\Api\Synthesis\SynthesisManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class OpenAKController
{
    public function test(OpenAKManager $openAKManager)
    {
        return $openAKManager->test();
    }
    public function navsInfo(string $navs, OpenAKManager $openAKManager)
    {
        return $openAKManager->navsInfo($navs);
    }

    public function retrievePensionCalculator(Response $response, OpenAKManager $openAKManager, SynthesisManager $synthesisManager)
    {

        try {
            $calc = $openAKManager->retrievePensionCalculator();
        } catch (\Exception $e) {
            $response->getBody()->write(
                json_encode(
                    (object) [
                    'info' => 'erreur du côté de OpenAK',
                    'msg'  => $e->getMessage(),
                    ]
                )
            );

            return $response;
        }

        $synthesisManager->manageOpenAKCalculator($calc);
        $response->getBody()->write(json_encode($calc));

        return $response;
    }

    public function getPensionCalculatorInfo(Response $response, OpenAKManager $openAKManager)
    {
        $info = $openAKManager->getPensionCalculatorInfo();
        $response->getBody()->write(json_encode($info));

        return $response;
    }

    public function postPensionCalculatorInfo(Request $request, Response $response, OpenAKManager $openAKManager)
    {
        $parsedBody = $request->getParsedBody();

        $navs          = $parsedBody['navs'];
        $birthDate     = $parsedBody['birthDate'];
        $sex           = $parsedBody['sex'];
        $relationships = $parsedBody['relationships'];

        $openAKManager->savePensionCalculatorInfo($navs, $birthDate, $sex, $relationships);

        return $response;
    }
}
