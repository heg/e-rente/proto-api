<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Exceptions\LoginException;
use Dibs\Api\Login\LoginManager;
use Dibs\Api\OpenPK\OpenPKManager;
use Dibs\Api\User\DbManager;
use Dibs\Api\User\UserManager;
use Predis\ClientInterface as RedisClient;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LoginController
{
    public function options(Response $response)
    {
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function loginViaPensionFund(Request $request, Response $response, LoginManager $loginManager, OpenPKManager $openpkManager, DbManager $dbManager)
    {
        $query         = $request->getQueryParams();
        $pensionFundId = $query['pension-fund-id'] ?? false;

        if (!$pensionFundId) {
            throw new LoginException("pension-fund-id introuvable dans la requête");
        }

        $_SESSION['LOGIN_OPENPK_ID'] = $pensionFundId;

        return $this->token($request, $response, $loginManager, $openpkManager, $dbManager);
    }

    public function me(Response $response, LoginManager $loginManager, OpenPKManager $openpkManager)
    {
        $loginManager  = $this->initLoginManager($loginManager, $openpkManager);
        $resourceOwner = $loginManager->getResourceOwner();

        $response->getBody()->write(json_encode($resourceOwner->toArray()));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function reset(Response $response, LoginManager $loginManager, UserManager $userManager, RedisClient $redisClient, OpenPKManager $openpkManager)
    {
        $userManager->reset();

        return $this->disconnect($response, $loginManager, $redisClient, $openpkManager);
    }

    protected function initLoginManager(LoginManager $loginManager, OpenPKManager $openpkManager): LoginManager
    {

        if ($_SESSION['LOGIN_OPENPK_ID'] ?? false) {
            $pensionFundId = $_SESSION['LOGIN_OPENPK_ID'];
            $registry      = $openpkManager->getRegistry(forLogin: true);
            $pensionFund   = $registry[$pensionFundId];

            if (!$pensionFund) {
                throw new LoginException("pensionFund correspondant à " . $pensionFundId . " introuvable");
            }

            $redirectUri = $loginManager->getOAuthProvider()->getRedirectUri();
            $newProvider = new \Dibs\Api\Login\OAuthProvider(
                [
                    'clientId'                => $openpkManager->getClientId($pensionFund),
                    'clientSecret'            => $openpkManager->getClientSecret($pensionFund),
                    'redirectUri'             => $redirectUri,
                    'logoutUri'               => $pensionFund->openpkServices->identityProvider . '/protocol/openid-connect/logout',
                    'urlAuthorize'            => $pensionFund->openpkServices->authorizeUrl,
                    'urlAccessToken'          => $pensionFund->openpkServices->tokenEndpoint,
                    'urlResourceOwnerDetails' => $pensionFund->openpkServices->resourceOwnerDetailUrl,
                ]
            );
            $loginManager->setOAuthProvider($newProvider);
        }

        return $loginManager;
    }

    public function token(Request $request, Response $response, LoginManager $loginManager, OpenPKManager $openpkManager, DbManager $dbManager)
    {
        $loginManager = $this->initLoginManager($loginManager, $openpkManager);

        $query   = $request->getQueryParams();
        $referer = $query['flash_uri'] ?? false;
        $code    = $query['code'] ?? false;
        $state   = $query['state'] ?? false;

        $redirect = $query['redirect'] ?? false;

        if ($redirect) {
            $_SESSION['REDIRECT'] = $redirect;
        }

        if (!$code) {
            if ($referer) {
                $_SESSION['flash_uri'] = $referer;
            }

            return $response
                ->withHeader('Location', $loginManager->getAuthorizationUrl())
                ->withStatus(302);
        }

        $referer = $_SESSION['flash_uri'] ?? null;

        if (!$loginManager->isStateOkay($state)) {
            $loginManager->cancelAuthorizationAttempt();
            $response->getBody()->write('Invalid state');

            return $response
                ->withStatus(401);
        }

        $loginManager->retrieveAccessToken($code);

        $_SESSION['flash_uri'] = null;

        $redirect             = $_SESSION['REDIRECT'] ?? null;
        $_SESSION['REDIRECT'] = null;

        $location = $referer ?: $_ENV['INTERFACE_DOMAIN'] . ($redirect ?: $_ENV['INTERFACE_LOGIN_SUCCESS_PATH']);

        if ($_SESSION['LOGIN_OPENPK_ID'] ?? false) {
            $resourceOwner = $loginManager->getResourceOwner();
            $dbManager->init($resourceOwner);
            $openpkManager->savePensionFundSelection([$_SESSION['LOGIN_OPENPK_ID']]);
            $location = '/openpk/user/pension-funds/tokens';
        }

        return $response
            ->withHeader('Location', $location)
            ->withStatus(302);
    }

    public function disconnect(Response $response, LoginManager $loginManager, RedisClient $redisClient, OpenPKManager $openpkManager)
    {
        $loginManager = $this->initLoginManager($loginManager, $openpkManager);

        $redisClient->set('fake_grant_is_ok', false);

        $referer = $_SERVER['HTTP_REFERER'] ?? false;
        $loginManager->emptySession();

        $_SESSION['REFERER'] = $_SERVER['HTTP_REFERER'] ?? false;

        return $response
            ->withHeader('Location', $loginManager->getLogoutUri())
            ->withStatus(302);
    }

    public function redirect(Response $response)
    {
        $referer = $_SESSION['REFERER'] ?: $_ENV['INTERFACE_DOMAIN'];

        return $response
            ->withHeader('Location', $referer)
            ->withStatus(302);
    }
}
