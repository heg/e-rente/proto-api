<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Exceptions\ValidationFailedException;
use Dibs\Api\OpenPV\OpenPVManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class OpenPVController
{
    public function grantingIsOkay(Twig $twig, Response $response)
    {
        return $twig->render($response, 'grant-ok.twig');
    }

    public function options(Response $response)
    {
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getRegistry(Response $response, OpenPVManager $openpvManager)
    {
        $registry = $openpvManager->getRegistry(true);
        $response->getBody()->write(json_encode($registry));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function setEntities(Request $request, Response $response, OpenPVManager $openpvManager)
    {
        $parsedBody = $request->getParsedBody();
        $ids        = $parsedBody['ids'] ?? [];

        if (is_string($ids)) {
            $ids = json_decode($ids);
        }

        if (!is_array($ids)) {
            throw new ValidationFailedException("'ids' doit être un tableau d'identifiant de caisses de pension'");
        }

        $openpvManager->saveSelection($ids);

        return $response;
    }

    public function getEntities(Response $response, OpenPVManager $openpvManager)
    {
        $ids = $openpvManager->getSelection();
        $response->getBody()->write(json_encode($ids));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getNextGrant(Request $request, Response $response, OpenPVManager $openpvManager)
    {
        $nextRegistryInfo = $openpvManager->getNextGrantRegistryInfo();

        if (!$nextRegistryInfo) {
            return $response
                ->withHeader('Location', '/openpv/user/grants/ok')
                ->withStatus(302);
        }

        $query     = $request->getQueryParams();
        $result    = $query['result'] ?? false;

        if (!$result) {
            return $response
                ->withHeader('Location', $openpvManager->getGrantUrl($nextRegistryInfo))
                ->withStatus(302);
        }

            return $response
                ->withHeader('Location', '/openpv/user/grants')
                ->withStatus(302);
    }

    public function getNextConsent(Request $request, Response $response, OpenPVManager $openpvManager)
    {
        $nextRegistryInfo = $openpvManager->getNextConsentRegistryInfo();

        if (!$nextRegistryInfo) {
            return $response
                ->withHeader('Location', '/openpv/user/grants')
                ->withStatus(302);
        }

        $query = $request->getQueryParams();
        $code  = $query['code'] ?? false;
        $state = $query['state'] ?? false;

        if (!$code) {
            $authorizationUrl = $openpvManager->getAuthorizationUrl($nextRegistryInfo);
            return $response
                ->withHeader('Location', $openpvManager->getAuthorizationUrl($nextRegistryInfo))
                ->withStatus(302);
        }
        if (!$openpvManager->isStateOkay($state)) {
            $response->getBody()->write('Invalid state');

            return $response
                ->withStatus(401);
        }

        $openpvManager->retrieveAccessToken($nextRegistryInfo, $code);

        return $response
            ->withHeader('Location', '/openpv/user/tokens')
            ->withStatus(302);
    }

    public function getProvidents(Response $response, OpenPVManager $openpvManager)
    {
        $providents = $openpvManager->getProvidents();

        $response->getBody()->write(json_encode($providents));

        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
    }
}
