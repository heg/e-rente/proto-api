<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\Synthesis\SynthesisManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class SynthesisController
{
    public function digest(Request $request, Response $response, SynthesisManager $SynthesisManager)
    {

        $query = $request->getQueryParams();

        $currentRate                    = isset($query['currentRate']) ? $query['currentRate'] : null;
        $futureRate                     = isset($query['futureRate']) ? $query['futureRate'] : null;
        $effectiveDate                  = isset($query['effectiveDate']) ? $query['effectiveDate'] : null;
        $futureSalary                   = isset($query['futureSalary']) ? $query['futureSalary'] : null;
        $currentSalary                  = isset($query['currentSalary']) ? $query['currentSalary'] : null;
        $nbYearBeforeAfterAgeRetirement = isset($query['nbYearBeforeAfterAgeRetirement']) ? $query['nbYearBeforeAfterAgeRetirement'] : null;

        $simulation = null;

        if ($futureRate || $effectiveDate || $futureSalary) {
            $simulation = new SimulationData(
                ...[
                    'currentRate'                    => $currentRate,
                    'futureRate'                     => $futureRate,
                    'effectiveDate'                  => $effectiveDate,
                    'currentSalary'                  => $currentSalary,
                    'futureSalary'                   => $futureSalary,
                    'nbYearBeforeAfterAgeRetirement' => $nbYearBeforeAfterAgeRetirement,
                ]
            );
        }

        $digestion = $SynthesisManager->digest($simulation);
        $response->getBody()->write(json_encode($digestion));

        return $response;
    }
}
