<?php

namespace Dibs\Api\Controllers;

use Predis\ClientInterface as RedisClient;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class OpenPVFakeImplentationController
{
    public function __construct(protected RedisClient $redisClient)
    {
    }

    public function getRegistry(Response $response)
    {
        $registry = [
            [
                "id"       => "fake1",
                "name"     => "Hypothekarbank",
                "nameDe"   => "Hypothekarbank",
                "address"  => "Hypothekarbank",
                "status"   => "active",
                "services" => [
                    "authorizeUrl"      => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation/protocol/openid-connect/auth",
                    "tokenEndpoint"     => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation/protocol/openid-connect/token",
                    "grantUrl"          => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation/protocol/openid-connect/grant",
                    "identityProvider"  => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation",
                    "consentEndpoint"   => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation",
                    "providentEndpoint" => $_ENV['OPENPV_FAKE_BANK_DOMAIN'] . "openpv-fake-implementation",
                    "providentQuery" => "https://inofwebdevapiopenpv.azurewebsites.net/openpv_accounts?ContractNr=[CONTRACT]",
                ],
            ],
        ];
        $response->getBody()->write(json_encode($registry));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    protected function getCurrentUser(): string
    {
        return $_COOKIE['user_fake_bank'];
    }

    protected function setCurrentUser(string $user): void
    {
        setcookie('user_fake_bank', strtolower($user));
    }

    public function auth(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();

        if ($query['logged_in_already'] ?? false) {
            $user = $this->setCurrentUser($query['code']);
            $url  = $query['redirect_uri'] . '?' . http_build_query($query);

            return $response
                ->withHeader('Location', $url)
                ->withStatus(302);
        }

        return $twig->render(
            $response,
            'openpvFakeImplementation/auth.twig',
            [
            'redirect_uri' => $query['redirect_uri'],
            'state'        => $query['state'],
            ]
        );
    }

    public function token(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $code = $body['code'];

        $oauth = [
            "token_type"    => "Bearer",
            "access_token"  => $code,
            "refresh_token" => $code,
            "expires"       => time() + (3600 * 24),
        ];
        $response->getBody()->write(json_encode($oauth));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function grant(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();
        $user  = $this->getCurrentUser();

        return $twig->render(
            $response,
            'openpvFakeImplementation/grant.twig',
            [
            'user'         => $user,
            'redirect_uri' => $query['redirect_uri'],
            'pension_id'   => $query['pension_id'],
            'client_id'    => $query['client_id'],
            ]
        );
    }

    protected function setGrant4User(string $user, bool $isOkay): void
    {
        $this->redisClient->set('fake_grant_is_ok:' . $user, $isOkay, 'EX', 3600);
    }

    protected function getGrant4User(string $user): bool
    {
        return $this->redisClient->get('fake_grant_is_ok:' . $user) ?: false;
    }

    public function grantIsOk(Twig $twig, Request $request, Response $response)
    {
        $query = $request->getQueryParams();
        $user  = $this->getCurrentUser();
        $this->setGrant4User($user, true);

        return $response
            ->withHeader('Location', $query['redirect_uri'])
            ->withStatus(302);
    }

    protected function getUserFromRequest(Request $request): string
    {
        $authorization = $request->getHeaderLine('Authorization');
        $authorization = explode(' ', $authorization);
        $user          = array_pop($authorization);

        return strtolower($user);
    }

    public function me(Request $request, Response $response)
    {
        $user      = $this->getUserFromRequest($request);
        $isGranted = $this->getGrant4User($user);

        $providentGrants = match ($user) {
            'alice' => [
                "providentAccessRight" => "read-only",
                "providentId"          => "CH794201",
            ],
            'beatrice' => [
                "providentAccessRight" => "read-only",
                "providentId"          => "CH798402",
            ],
            'dan' => [
                "providentAccessRight" => "read-only",
                "providentId"          => "CH792103",
            ],
        };

        if ($isGranted) {
            $response->getBody()->write(
                json_encode(
                    [
                    "personGrant"     =>
                    [
                    "personAccessRight" => "read-only",
                    "personId"          => "93816732",
                    ],
                    "providentGrants" =>
                    [
                    $providentGrants
                    ],
                    ]
                )
            );
        } else {
            $response->getBody()->write(json_encode([]));
        }

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    // public function providents(Request $request, Response $response)
    // {
    //     $user = $this->getUserFromRequest($request);

    //     $values = match ($user) {
    //         'alice' => [
    //             "id"      => $user,
    //             "amount"  => 0,
    //             "paydate" => "01.11.2027",
    //             "rate"    => 0,
    //             "solde"   => 100000,
    //         ],
    //         'beatrice' => [
    //             "id"      => $user,
    //             "amount"  => 0,
    //             "paydate" => "01.11.2027",
    //             "rate"    => 0,
    //             "solde"   => 10000,
    //         ],
    //         'dan' => [
    //             "id"      => $user,
    //             "amount"  => 0,
    //             "paydate" => "01.11.2027",
    //             "rate"    => 0,
    //             "solde"   => 30000,
    //         ],
    //         'françoise' => [
    //             "id"      => $user,
    //             "amount"  => 0,
    //             "paydate" => "01.11.2027",
    //             "rate"    => 0,
    //             "solde"   => 20000,
    //         ],
    //         default => [
    //             "id"      => $user,
    //             "amount"  => 1000,
    //             "paydate" => "01.11.2027",
    //             "rate"    => 0.15,
    //             "solde"   => 10000,
    //         ]
    //     };

    //     $response->getBody()->write(json_encode($values));

    //     return $response
    //         ->withHeader('Content-Type', 'application/json');
    // }
}
