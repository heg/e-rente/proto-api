<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Open3K\Open3KManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Open3KController
{
    public function getInfo(Request $request, Response $response, Open3KManager $open3KManager)
    {
        $info = $open3KManager->getInfo();
        $response->getBody()->write(json_encode($info));

        return $response;
    }

    public function postInfo(Request $request, Response $response, Open3KManager $open3KManager)
    {
        $parsedBody = $request->getParsedBody();

        $solde   = $parsedBody['solde'] ?? null;
        $amount  = $parsedBody['amount'] ?? null;
        $rate    = $parsedBody['rate'] ?? null;
        $paydate = $parsedBody['paydate'] ?? null;
        $payoutPredication = $parsedBody['payout_predication'] ?? null;

        $open3KManager->saveInfo($solde, $amount, $rate, $paydate, $payoutPredication);

        return $response;
    }
}
