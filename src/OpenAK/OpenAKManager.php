<?php

namespace Dibs\Api\OpenAK;

use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\User\UserManager;
use GuzzleHttp\ClientInterface as HttpClient;

class OpenAKManager
{
    public function __construct(HttpClient $httpClient, UserManager $userManager)
    {
        $this->httpClient  = $httpClient;
        $this->userManager = $userManager;
    }

    public function reset()
    {
        $this->savePensionCalculatorInfo(null, null, null, null);
    }

    public function test()
    {
        return $this->httpClient->request('GET', '/api/info');
    }

    public function navsInfo($navs)
    {
        return $this->httpClient->request('GET', "/api/insureds/{$navs}");
    }

    public function getPensionCalculatorInfo()
    {
        $info = $this->userManager->retrieveFromPrivateDb('openak');

        return [
            'navs'          => $info['navs'],
            'birthDate'     => $info['birthDate'],
            'sex'           => $info['sex'],
            'relationships' => $info['relationships'],
        ];
    }

    public function savePensionCalculatorInfo($navs, $birthDate, $sex, $relationships)
    {
        $this->userManager->appendToPrivateDb('openak.navs', $navs);
        $this->userManager->appendToPrivateDb('openak.birthDate', $birthDate);
        $this->userManager->appendToPrivateDb('openak.sex', $sex);
        $this->userManager->appendToPrivateDb('openak.relationships', $relationships);
        $this->userManager->appendToPrivateDb('openak.calculator', null);
    }

    /**
     * Récupérer les informations de pension
     *
     * @return GuzzleHttp\Psr7\Response Réponse de l'api d'OpenPK
     */
    public function retrievePensionCalculator(SimulationData $simulationData = null)
    {
        $calculator = $this->userManager->retrieveFromPrivateDb('openak');

        if ($simulationData || !$calculator || !$calculator['calculator']) {
            try {
                $calculator = $this->postPensionCalculatorResponse($simulationData);
            } catch (\Exception $e) {
                return null;
            }

            $calculator = json_decode($calculator->getBody(), true, 512, JSON_OBJECT_AS_ARRAY);

            // echo json_encode([$calculator]);

            // die();
            if (!$simulationData) {
                $this->userManager->appendToPrivateDb('openak.calculator', $calculator);
            }
        } else {
            $calculator = $calculator['calculator'];
        }

        return $calculator;
    }

    /**
     * Récupérer les informations de pension
     *
     * @return GuzzleHttp\Psr7\Response Réponse de l'api d'OpenPK
     */
    public function postPensionCalculatorResponse(SimulationData $simulationData = null)
    {
        $data = $this->userManager->retrieveFromPrivateDb('openak');
        $navs = $data['navs'];
        if (!$navs) {
            throw new \Exception('Pas de NAVS', 401);
        }

        $birthDate     = $data['birthDate'];
        $sex           = strtoupper($data['sex']);
        $calculator    = $data['calculator'];
        $relationships = $data['relationships'];
        if ($relationships) {
            $relationships = array_filter(
                $relationships,
                function ($relation) {
                    return $relation['type'] != 'CHILDREN';
                }
            );
        }

        $simArgs = [
            'navs'                           => $data['navs'],
            'nbYearBeforeAfterAgeRetirement' => 0,
        ];

        if ($simulationData->futureSalary == $simulationData->currentSalary) {
            $simArgs['currentRate'] = $simulationData ? (int) $simulationData->currentRate : null;
            $simArgs['futureRate']  = $simulationData ? (int) $simulationData->futureRate : null;
        } else {
            $simArgs['futureSalary'] = $simulationData ? (int) $simulationData->futureSalary : null;
        }
        $simArgs['nbYearBeforeAfterAgeRetirement'] = $simulationData ? (int) $simulationData->nbYearBeforeAfterAgeRetirement : 0;

        $query = [
            'applicant'     =>
            [
                'navs'        => $navs,
                'birthDate'   => $birthDate,
                // 'deathDate' => '',
                'nationality' => 100,
                'residency'   => 100,
                'sex'         => $sex,
            ],
            'relationships' => $relationships,
            'insureds'      =>
            [
                0 =>
                $simArgs,
            ],
        ];

        /*        $query = [
        'applicant' => [
        'navs' => 7560000000019,
        'birthDate' => '1982-02-01',
        'nationality' => 100,
        'residency' => 100,
        'sex' => 'FEMALE',
        ],
        'relationships' => [
        0 => [
        'start' => '2000-11-02',
        'type' => 'SPOUSE',
        'person' => [
        'navs' => 7560000000026,
        'birthDate' => '1983-02-04',
        'nationality' => 100,
        'residency' => 100,
        'sex' => 'MALE',
        ],
        // 'navsParent' => '',
        ],
        ],
        'insureds' => [
        // 0 => [
        //   'navs' => 0,
        //   'nbYearBeforeAfterAgeRetirement' => 0,
        //   'futureRate' => 100,
        // ],
        ],
        ];*/

        // echo json_encode($query);
        // die();

        return $this->httpClient->request(
            'POST',
            'pension-calculator',
            [
                'json' => $query,
            ]
        );
    }
}
