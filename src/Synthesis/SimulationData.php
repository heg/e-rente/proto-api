<?php

namespace Dibs\Api\Synthesis;

class SimulationData
{
    public function __construct(
        public ?string $currentRate,
        public ?string $futureRate,
        public ?string $effectiveDate,
        public ?string $currentSalary,
        public ?string $futureSalary,
        public ?string $nbYearBeforeAfterAgeRetirement
    ) {
    }
}
