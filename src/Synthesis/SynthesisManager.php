<?php

namespace Dibs\Api\Synthesis;

use Dibs\Api\Open3K\Open3KManager;
use Dibs\Api\OpenAK\OpenAKManager;
use Dibs\Api\OpenPK\OpenPKManager;
use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\User\UserManager;

class SynthesisManager
{
    public function __construct(protected Open3KManager $open3KManager, protected OpenPKManager $openPKManager, protected OpenAKManager $openAKManager, protected UserManager $userManager)
    {
    }

    public function digest(SimulationData $simulation = null)
    {
        $digest    = [];
        $annuities = 0;
        $capital   = 0;

        try {
            $openAk          = $this->openAKManager->retrievePensionCalculator($simulation);
            $originalPension = $openAk['originalPension'];
            $navs            = $this->userManager->retrieveFromPrivateDb('openak.navs');
        } catch (\TypeError  | \Exception $e) {
            $originalPension = null;
            $openAk          = null;
            $navs            = null;
        }

        try {
            $openPK = $this->openPKManager->getPolicies($simulation);
        } catch (\TypeError  | \Exception $e) {
            $openPK = null;
        }

        $open3K = $this->open3KManager->getInfo();

        if ($originalPension) {
            $periodPensions = [];

            foreach ($originalPension as $i => $pensions) {
                $date = $pensions['date'];

                if (isset($periodPensions[$i - 1])) {
                    $periodPensions[$i - 1]['end'] = $date;
                }

                $periodPensions[$i]['pensions'] = array_map(
                    function ($pension) use ($navs) {
                        $pension['is_me'] = $pension['navs'] == $navs;
                        return $pension;
                    },
                    $pensions['pensions']
                );

                usort($periodPensions[$i]['pensions'], fn($a, $b) => ($a['is_me'] < $b['is_me']) ? -1 : 1);

                $periodPensions[$i]['start'] = $date;
                $periodPensions[$i]['end']   = null;

                array_map(
                    function ($pension) use ($navs) {
                        $pension['is_me'] = $pension['navs'] == $navs;
                        return $pension;
                    },
                    $periodPensions[$i]['pensions'],
                );
            }

            // on récupère la toute première pension m'appartenant pour l'ajouter aux rentes
            $pensionsFilteredByMe = array_filter(
                reset($periodPensions)['pensions'],
                fn($pension) => $pension['is_me']
            );
            $annuities += end($pensionsFilteredByMe)['amount'] ?? 0;

            $digest['avs_pensions_by_period'] = $periodPensions;
        }

        if ($openPK) {
            $openPkAnnuities = array_map(
                function ($pk) {
                    $pk       = json_decode(json_encode($pk), true);
                    $benefits = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                    return [
                        'annuities' => end($benefits)['pension'] / 12,
                        'capital'   => end($benefits)['capitalBalance'],
                        'age'       => end($benefits)['age'],
                    ];
                },
                $openPK,
            );
            $digest['lpp_pensions_by_age'] = $openPkAnnuities;

            $annuities += array_reduce(
                $openPkAnnuities,
                function ($carry, $pk) {
                    return $carry + $pk['annuities'];
                },
                0
            );
        }

        if ($open3K) {
            if ($open3K['payout_predication']) {
                $open3K['term'] = $open3K['payout_predication'];
                // on ne prend plus en compte la prediction d'open3k dans le capital final
                $capital += $open3K['solde'];
                $digest['3a'] = $open3K;
            } elseif ($open3K['solde'] || $open3K['amount'] || $open3K['rate']) {
                $today             = new \DateTime("now");
                $payment           = new \DateTime($open3K['paydate']);
                $yearDiff          = $payment->diff($today)->format("%y");
                $open3K['term']    = $open3K['solde'] + ($open3K['amount'] * $yearDiff) + ($open3K['amount'] * $yearDiff * $open3K['rate'] / 100);
                $open3K['monthly'] = $open3K['amount'] / 12;

                $capital += $open3K['term'];
                $digest['3a'] = $open3K;
            }
        }

        $digest['annuities'] = $annuities;
        $digest['capital']   = $capital;

        return $digest;
    }

    public function digestOld(SimulationData $simulation = null)
    {
        $digest = [
            'date'      => null,
            'annuities' => [
                '1_pillar' => 0,
                '2_pillar' => 0,
            ],
            'capitals'  => [
                '2_pillar'        => 0,
                '3_pillar'        => 0,
                'vested_benefits' => 0,
            ],
        ];

        $navs = $this->userManager->retrieveFromPrivateDb('openak.navs');
        $navs = trim($navs);
        try {
            $openAK = $this->openAKManager->retrievePensionCalculator($simulation);
        } catch (\Exception $e) {
        }

        $openPK = $this->openPKManager->getPolicies($simulation);
        $open3K = $this->open3KManager->getInfo();

        $digest['date'] = [
            'value' => $this->open3KManager->getPaydate(),
            'meta'  => [
                'description' => 'Valeur entrée lors du formulaire du 3ème pilier',
                'formula'     => '= open3K.payDate',
            ],
        ];
        $digest['annuities']['1_pillar'] = [
            // 'value' => end($openAK['originalPension'])['pension']['amount'],
            'value' => isset($openAK['originalPension']) ? array_column(end($openAK['originalPension'])['pensions'], 'amount', 'navs')[$navs] : 0,
            'meta'  => [
                'description' => 'Valeur de rente personnelle retournée par OpenAK',
                'formula'     => '= openAK.originalPension.pension.amount',
            ],
        ];

        $digest['annuities']['2_pillar'] = [
            'value' => array_reduce(
                $openPK,
                function ($sum, $pk) {
                    $pk       = json_decode(json_encode($pk), true);
                    $benefits = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                    return $sum + end($benefits)['pension'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des rentes retournées par OpenPK',
                'formula'     => isset($pk['retirementCapital']) ? '= sum(openPK.retirementCapital.projectedRetirementBenefits.pension)' : '= sum(openPK.projectedRetirementBenefits.pension)',
                'variables'   => array_reduce(
                    $openPK,
                    function ($vars, $pk) {
                        $pk                                   = json_decode(json_encode($pk), true);
                        $benefits                             = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                        $vars['rente ' . (sizeof($vars) + 1)] = end($benefits)['pension'];
                        return $vars;
                    },
                    []
                ),
            ],
        ];

        $digest['annuities']['total'] = [
            'value' => $digest['annuities']['1_pillar']["value"] + $digest['annuities']['2_pillar']["value"],
            'meta'  => [
                'description' => 'Somme des rentes',
                'formula'     => '= annuities.1_pillar + annuities+2_pillar',
            ],
        ];

        $digest['capitals']['2_pillar'] = [
            'value' => array_reduce(
                $openPK,
                function ($sum, $pk) {
                    $pk       = json_decode(json_encode($pk), true);
                    $benefits = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                    return $sum + end($benefits)['capitalBalance'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des capitaux maximum retournés par OpenPK',
                'formula'     => isset($pk['retirementCapital']) ? '= sum(openPK.retirementCapital.projectedRetirementBenefits.capitalBalance)' : '= sum(openPK.projectedRetirementBenefits.capitalBalance)',
                'variables'   => array_reduce(
                    $openPK,
                    function ($vars, $pk) {
                        $pk                                     = json_decode(json_encode($pk), true);
                        $benefits                               = isset($pk['retirementCapital']) ? $pk['retirementCapital']['projectedRetirementBenefits'] : $pk['projectedRetirementBenefits'];
                        $vars['capital ' . (sizeof($vars) + 1)] = end($benefits)['capitalBalance'];
                        return $vars;
                    },
                    []
                ),
            ],
        ];

        $today    = new \DateTime("now");
        $payment  = new \DateTime($this->open3KManager->getPaydate());
        $yearDiff = $payment->diff($today)->format("%y"); //3

        $digest['capitals']['3_pillar'] = [
            'value' => $open3K['solde'] + ($open3K['amount'] * $yearDiff) + ($open3K['amount'] * $yearDiff * $open3K['rate'] / 100),
            'meta'  => [
                'description' => "Formule basée sur les valeurs entrées dans le formulaire Open3K et multiplié par le nombre d'années séparant l'utilisateur de sa retraite",
                'formula'     => '= open3K.solde + (open3K.amount * yearDiff) + (open3K.amount * yearDiff * open3K.rate / 100)',
                'variables'   => [
                    'open3K.solde'    => $open3K['solde'],
                    'open3K.amount'   => $open3K['amount'],
                    'open3K.rate'     => $open3K['rate'],
                    'yearDiff'        => $yearDiff,
                    'today'           => $today->format('d/m/Y'),
                    'date_of_payment' => $payment->format('d/m/Y'),
                ],
            ],
        ];

        $digest['capitals']['vested_benefits'] = [
            'value' => $open3K['vested_benefits'] ?? 0,
            'meta'  => [
                'description' => 'Valeur entrée lors du formulaire du 3ème pilier',
                'formula'     => '= open3K.vested_benefits',
            ],
        ];

        $digest['capitals']['total'] = [
            'value' => array_reduce(
                $digest['capitals'],
                function ($sum, $capital) {
                    return $sum + $capital['value'];
                },
                0
            ),
            'meta'  => [
                'description' => 'Somme des capitaux',
                'formula'     => '= capitals.2_pillar + capitals.3_pillar + capitals.vested_benefits',
            ],
        ];

        return $digest;
    }

    public function manageOpenAKCalculator($openAKResponse)
    {
        // $data = json_decode($openAKResponse->getBody());
        $data = $openAKResponse;

        if (!isset($data['originalPension'])) {
            return;
        }

        $date = current($data['originalPension'])['date'] ?? false;

        if (!$date) {
            return;
        }

        $open3k = (array) $this->open3KManager->getInfo();

        if (!$open3k || !$open3k['solde']) {
            $this->open3KManager->savePaydate($date);
        }
    }
}
