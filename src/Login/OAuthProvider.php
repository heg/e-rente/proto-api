<?php

namespace Dibs\Api\Login;

use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Token\AccessToken;

class OAuthProvider extends GenericProvider
{
    protected $logoutUri;
    protected $redirectUri;

    public function __construct($data)
    {
        $this->logoutUri   = $data['logoutUri'];
        $this->redirectUri = $data['redirectUri'];

        return parent::__construct($data);
    }

    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    public function getLogoutUri()
    {
        $protocol    = strpos(strtolower($_SERVER['INTERFACE_DOMAIN']), 'https') === false ? 'http' : 'https';
        $redirectUri = ($_ENV['REDIRECT_DOMAIN'] ?? ($protocol . '://' . $_SERVER['HTTP_HOST'])) . '/redirect';

        $logoutUri       = explode('?', $this->logoutUri);
        $explodedQueries = explode('&', $logoutUri[1]);

        foreach ($explodedQueries as $queryPair) {
            $pair            = explode('=', $queryPair);
            $query[$pair[0]] = $pair[1];
        }

        $logoutUri = $logoutUri[0] . '?' . http_build_query(array_merge([
            'returnTo'     => $redirectUri,
            'redirect_uri' => $redirectUri,
        ], $query));

        return $logoutUri;
    }

    /**
     * @inheritdoc
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new GenericResourceOwner($response, $_ENV['LOGIN_RESOURCE_OWNER_ID']);
    }
}
