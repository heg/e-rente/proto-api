<?php

namespace Dibs\Api\OpenPK;

use Dibs\Api\Data\DataManager;
use Dibs\Api\Exceptions\UnauthorizedException;
use Dibs\Api\OpenPk\OAuthProviderFactory;
use Dibs\Api\Synthesis\SimulationData;
use Dibs\Api\User\UserManager;
use GuzzleHttp\ClientInterface as HttpClient;
use League\OAuth2\Client\Token\AccessToken;

class OpenPKManager
{
    public function __construct(protected HttpClient $httpClient, protected DataManager $dataManager, protected UserManager $userManager, protected OAuthProviderFactory $oAuthProviderFactory)
    {
    }

    public function manageOpenPKLoginData($pensionFundId)
    {
        $registry      = $this->getRegistry(forLogin: true);
        $pensionFund   = $registry[$pensionFundId];
        $resourceOwner = $this->getResourceOwner($pensionFund);

        if ($resourceOwner) {
            $resourceOwner = $resourceOwner->toArray();
            $email         = $resourceOwner['email'] ?? false;
            $navs          = match ($email) {
                'alice@dibs' => '7560000000002',
                'beatrice@dibs' => '7560000000019',
                default => null,
            };

            if ($navs) {
                $this->userManager->appendToPrivateDb('openak.navs', $navs);
            }
        }
    }

    public function reset()
    {
        $this->savePensionFundSelection(null);
    }

    public function savePensionFundSelection($ids)
    {
        $this->userManager->appendToPrivateDb('openpk.pensions.selection', $ids);
        $this->userManager->appendToPrivateDb('openpk.policies', null);
        $this->userManager->appendToPrivateDb('openpk.pensions.consents', null);
    }

    public function getPensionFundSelection()
    {
        return $this->userManager->retrieveFromPrivateDb('openpk.pensions.selection');
    }

    public function getPensionFundGrants()
    {
        $grants = $this->userManager->retrieveFromPrivateDb('openpk.pensions.grants');
        return $grants ? json_decode($grants, true) : [];
    }

    public function getPensionFundTokens()
    {
        $tokens = $this->userManager->retrieveFromPrivateDb('openpk.pensions.consents');
        return $tokens ? json_decode($tokens, true) : [];
    }

    public function getPensionFundUserData()
    {
        return $this->userManager->retrieveFromPrivateDb('openpk.pensions') ?? [];
    }

    public function getRegistry($filtered = false, $forLogin = false)
    {
        $registry = $this->dataManager->get('openpk::registry');

        if (!$registry) {
            $response = $this->httpClient->request('GET', 'pension-funds');
            $registry = (string) $response->getBody();

            $filteredRegistry = [];

            foreach (json_decode($registry) as $key => $value) {
                if (
                    $value->openpkStatus == 'active'
                    && $value->id != 'TST-100'
                    && $value->id != 'TST-007'
                ) {
                    $filteredRegistry[] = $value;
                }
            }

            $registry = json_encode($filteredRegistry);
            $this->dataManager->set('openpk::registry', $registry, 60 * 30);
        }

        $registry = json_decode($registry);

        if ($forLogin) {
            $registry = array_filter(
                $registry,
                fn($pension) => $pension->openpkServices->resourceOwnerDetailUrl ?? false
            );
        }

        if ($filtered) {
            $registry = array_map(
                function ($pension) {
                    unset($pension->openpkServices);
                    unset($pension->openpkStatus);
                    unset($pension->address);

                    return $pension;
                },
                (array) $registry
            );
        }

        $registry = array_combine(array_column($registry, 'id'), $registry);
        return $registry;
    }

    public function getNextConsentRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getPensionFundSelection();
        $tokens    = $this->getPensionFundTokens();

        $toBeConsented = array_filter(
            $selection,
            function ($id) use ($tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testTokenIsValid($info, $token);
            }
        );

        return sizeof($toBeConsented) > 0 ? $registry[array_shift($toBeConsented)] : null;
    }

    public function getNextGrantRegistryInfo()
    {
        $registry  = $this->getRegistry();
        $selection = $this->getPensionFundSelection();
        $grants    = $this->getPensionFundGrants();
        $tokens    = $this->getPensionFundTokens();

        $toBeGranted = array_filter(
            $selection,
            function ($id) use ($grants, $tokens, $registry) {
                $token = $tokens[$id] ?? null;
                $info  = $registry[$id];

                return !$this->testGrantIsValid($info, $token);
            }
        );

        return sizeof($toBeGranted) > 0 ? $registry[array_shift($toBeGranted)] : null;
    }

    public function getGrantUrl($registryInfo)
    {
        $oAuthProvider    = $this->getOAuthProvider($registryInfo);
        $authorizationUrl = $oAuthProvider->getGrantUrl($registryInfo);

        return $authorizationUrl;
    }

    public function getAuthorizationUrl($registryInfo)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);

        $id    = $registryInfo->id;
        $scope = $_ENV['OPENPK_SCOPE_' . $id] ?? $_ENV['OPENPK_SCOPE'];

        $authorizationUrl                 = $oAuthProvider->getAuthorizationUrl(['scope' => $scope]);
        $_SESSION['openPKOauth2state']    = $oAuthProvider->getState();
        $_SESSION['openPKOauth2pkceCode'] = $oAuthProvider->getPkceCode();

        return $authorizationUrl;
    }

    public function isStateOkay(string $state = null)
    {
        return $state
        && isset($_SESSION['openPKOauth2state'])
            && ($state == $_SESSION['openPKOauth2state']);
    }

    public function getPolicies(SimulationData $simulationData = null)
    {
        $policies = $this->userManager->retrieveFromPrivateDb('openpk.policies');

        if ($simulationData || !$policies) {
            $registry = $this->getRegistry();
            $policies = array_reduce(
                $this->getPensionFundSelection() ?? [],
                function ($policies, $pensionId) use ($registry, $simulationData) {

                    if (!isset($registry[$pensionId])) {
                        return $policies;
                    }

                    $pensionInfo = $registry[$pensionId];
                    $accessToken = $this->getValidAccessToken($pensionInfo);

                    $oAuthProvider = $this->getOAuthProvider($pensionInfo);
                    $request       = $oAuthProvider->getAuthenticatedRequest('GET', $pensionInfo->openpkServices->policyEndpoint . '/users/me', $accessToken);
                    $response      = $oAuthProvider->send($request);
                    $grant         = json_decode($response->getBody()->getContents());

                    if ($simulationData) {
                        $data = [
                            "declaredSalary"  => $simulationData->futureSalary * $simulationData->futureRate / $simulationData->currentRate,
                            "employmentLevel" => 100 * $simulationData->futureRate / $simulationData->currentRate,
                        ];

                        $request = $oAuthProvider->getAuthenticatedRequest(
                            'POST',
                            $pensionInfo->openpkServices->policyEndpoint . '/policies/' . $grant->policyGrants[0]->policyId . '/salary-change-simulation',
                            $accessToken,
                            [
                                'body'    => json_encode($data),
                                'headers' => [
                                    'Content-Type' => 'application/json',
                                ],
                            ]
                        );
                    } else {
                        $request = $oAuthProvider->getAuthenticatedRequest('GET', $pensionInfo->openpkServices->policyEndpoint . '/policies/' . $grant->policyGrants[0]->policyId, $accessToken);
                    }

                    $response             = $oAuthProvider->send($request);
                    $policies[$pensionId] = json_decode($response->getBody()->getContents());

                    return $policies;
                },
                []
            );

            if (!$simulationData) {
                $this->userManager->appendToPrivateDb('openpk.policies', json_encode($policies));
            }
        } else {
            $policies = json_decode($policies, true);
        }

        return $policies;
    }

    public function getValidAccessToken($registryInfo)
    {
        $tokens   = $this->getPensionFundTokens();
        $policyId = $registryInfo->id;

        if (!isset($tokens[$policyId])) {
            throw new UnauthorizedException("Les consentements de la pension " . $policyId . " n'ont pas encore été donnés");
        }

        $accessToken = $this->testTokenIsValid($registryInfo, $tokens[$policyId]);

        if (!$accessToken) {
            unset($tokens[$policyId]);
            $this->userManager->appendToPrivateDb('openpk.pensions.consents', json_encode($tokens));
            throw new UnauthorizedException("Les tokens de la pension " . $policyId . " ne sont plus valides");
        }

        return $accessToken;
    }

    protected function testTokenIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        return $accessToken;
    }

    protected function testGrantIsValid($registryInfo, $token)
    {

        if (!$token) {
            return false;
        }

        $accessToken = new AccessToken($token);

        if ($accessToken->hasExpired()) {
            $accessToken = $this->refreshAccessToken($registryInfo, $accessToken);
        }

        $oAuthProvider = $this->getOAuthProvider($registryInfo);

        try {
            $resourceOwner = $oAuthProvider->getResourceOwner($accessToken);
            $resourceOwner = $resourceOwner->toArray();
        } catch (\UnexpectedValueException $e) {
            $resourceOwner = [];
        }

        return $resourceOwner['personGrant'] ?? false;
    }

    public function getOAuthProvider($registryInfo)
    {
        $secret   = $this->getClientSecret($registryInfo);
        $clientId = $this->getClientId($registryInfo);

        return $this->oAuthProviderFactory->getOAuthProvider($registryInfo, $clientId, $secret);
    }

    public function getClientId($registryInfo)
    {
        $id = $registryInfo->id;
        return $_ENV['OPENPK_CLIENT_ID_' . $id] ?? $_ENV['OPENPK_CLIENT_ID'];
    }

    public function getClientSecret($registryInfo)
    {
        $id = $registryInfo->id;
        return $_ENV['OPENPK_CLIENT_SECRET_' . $id] ?? $_ENV['OPENPK_CLIENT_SECRET'];
    }

    protected function refreshAccessToken($registryInfo, AccessToken $accessToken)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);

        try {
            $newAccessToken = $oAuthProvider->getAccessToken(
                'refresh_token',
                [
                    'refresh_token' => $accessToken->getRefreshToken(),
                ]
            );
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de rafraichir le token: ' . $e->getMessage());
        }

        $this->storeAccessToken($registryInfo, $newAccessToken);

        return $newAccessToken;
    }

    public function retrieveAccessToken($registryInfo, string $code)
    {
        $oAuthProvider = $this->getOAuthProvider($registryInfo);
        $oAuthProvider->setPkceCode($_SESSION['openPKOauth2pkceCode']);
        $accessToken = $oAuthProvider->getAccessToken(
            'authorization_code',
            [
                'code' => $code,
            ]
        );

        $this->storeAccessToken($registryInfo, $accessToken);
    }

    protected function storeAccessToken($registryInfo, AccessToken $accessToken)
    {
        $tokens                    = $this->getPensionFundTokens();
        $tokens[$registryInfo->id] = $accessToken;
        $this->userManager->appendToPrivateDb('openpk.pensions.consents', json_encode($tokens));
        $this->userManager->appendToPrivateDb('openpk.policies', null);
    }

    public function getResourceOwner($registryInfo)
    {
        $accessToken = $this->getValidAccessToken($registryInfo);

        try {
            if (!$this->resourceOwner) {
                $this->resourceOwner         = $_SESSION['ressource_owner'] ?? $this->oAuthProvider->getResourceOwner($accessToken);
                $_SESSION['ressource_owner'] = $this->resourceOwner;
            }
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de récupérer le propriétaire du token d\'accès: ' . $e->getMessage());
        }

        return $this->resourceOwner;
    }
}
