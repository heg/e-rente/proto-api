<?php

namespace Dibs\Api\OpenPk;

use Dibs\Api\OpenPK\OAuthProvider;

class OAuthProviderFactory
{
    protected $clientId;
    protected $clientSecret;

    public function __construct(protected $redirectUri, protected $redirectGrantUri, protected $timeout)
    {
    }

    public function getOAuthProvider($openPKRegistryInfo, $clientId, $secret)
    {
        $services = $openPKRegistryInfo->openpkServices;

        return new OAuthProvider(
            [
                'pkceMethod'              => \League\OAuth2\Client\Provider\GenericProvider::PKCE_METHOD_S256,
                'timeout'                 => $this->timeout,
                'clientId'                => $clientId,
                'clientSecret'            => $secret,
                'redirectUri'             => $this->redirectUri,
                'redirectGrantUri'        => $this->redirectGrantUri,
                'urlAuthorize'            => $services->authorizeUrl,
                'urlAccessToken'          => $services->tokenEndpoint,
                'urlResourceOwnerDetails' => $services->consentEndpoint . '/users/me',
            ]
        );
    }
}
