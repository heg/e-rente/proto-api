<?php

namespace Dibs\Api\OpenPK;

use League\OAuth2\Client\Provider\GenericProvider;
use Psr\Http\Message\RequestInterface;

class OAuthProvider extends GenericProvider
{
    protected $redirectGrantUri;
    protected $timeout;

    /**
     * @param array $options
     * @param array $collaborators
     */
    public function __construct(array $options = [], array $collaborators = [])
    {
        $this->redirectGrantUri = $options['redirectGrantUri'] ?? '';
        $this->timeout = $options['timeout'] ?? 0;

        parent::__construct($options, $collaborators);
    }

    public function send(RequestInterface $request)
    {
        return $this->getHttpClient()->send($request, ['timeout' => $this->timeout, 'connection_timeout' => $this->timeout]);
    }

    public function getGrantUrl($registryInfo)
    {
        $uri = $registryInfo->openpkServices->grantUrl;
        $id     = $registryInfo->id;
        $clientId = $_ENV['OPENPK_CLIENT_ID_' . $id] ?? $_ENV['OPENPK_CLIENT_ID'];

        $uri = parse_url($uri);
        $uri = $uri['scheme'] . '://' . $uri['host'] . $uri['path'] . '?'
        . (isset($uri['query']) ? $uri['query'] . '&' : '')
        . 'redirect_uri=' . urlencode($this->redirectGrantUri)
        . '&' . http_build_query(
            [
            'pension_id' => $registryInfo->id,
            'client_id'  => $clientId,
            ]
        );

        return $uri;
    }
}
