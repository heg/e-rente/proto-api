<?php

namespace Dibs\Api\Exceptions;

use Throwable;

/**
 * Exception lancée par DIBS
 */
abstract class AbstractException extends \Exception
{
    /**
     * Le code par défaut de l'exception
     *
     * @var int
     */
    const CODE = 0;

    /**
     * Code HTTP qui sera associé à la réponse
     *
     * @var int
     */
    public $httpCode = 500;

    /**
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     * @param int            $httpCode
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null, $httpCode = 500)
    {

        if ($code == 0) {
            $code = get_called_class()::CODE;
        }

        parent::__construct(
            json_encode(
                [
                'message' => $message,
                'trace'   => array_slice(self::getTrace(), 0, 10),
                ]
            ),
            $code,
            $previous
        );
        $this->setHttpCode($httpCode);
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @param int $httpCode
     */
    public function setHttpCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }
}
