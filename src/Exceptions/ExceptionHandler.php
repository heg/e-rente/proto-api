<?php

namespace Dibs\Api\Exceptions;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;

class ExceptionHandler
{
    public static function getHandler(App $app)
    {
        return function (
            ServerRequestInterface $request,
            \Throwable $exception,
            bool $displayErrorDetails,
            bool $logErrors,
            bool $logErrorDetails,
            ?LoggerInterface $logger = null
        ) use ($app) {
            $className = get_class($exception);
            $payload   = json_decode($exception->getMessage());

            $response = $app->getResponseFactory()->createResponse();
            $error    = [
                'message' => $payload->message ?? $exception->getMessage(),
                'status'  => $exception->getCode(),
            ];

            if ($displayErrorDetails) {
                $error['error'] = $className;
                $error['trace'] = array_slice($exception->getTrace(), 0, 10);
            }

            $response->getBody()->write(
                json_encode($error)
            );

            return $response->withStatus($exception->getCode() ?: 500)
                ->withHeader('Content-Type', 'application/json')
                ->withHeader('Access-Control-Allow-Origin', $_ENV['INTERFACE_DOMAIN'])
                ->withHeader('Access-Control-Allow-Credentials', 'true')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
        };
    }
}
