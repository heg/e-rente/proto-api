<?php

namespace Dibs\Api\Exceptions;

use Dibs\Api\Exceptions\AbstractException;

/**
 * Exception lancée si les formats des données est mauvais
 */
class LoginException extends AbstractException
{
    /**
     * Code de l'erreur: 422
     */
    const CODE = 401;
}
