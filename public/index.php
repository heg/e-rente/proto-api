<?php

require __DIR__ . '/../vendor/autoload.php';
use Dibs\Api\Exceptions\ExceptionHandler;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();
$isDebugMode = ($_ENV['SLIM_MODE'] ?? false) == 'debug';

set_time_limit(10);
ini_set('max_execution_time', 10);

// Configure PHP-DI container
$builder = new \DI\ContainerBuilder();
/**
 * Si jamais on veut compiler l'injection de code => en prod uniquement et
 * il faut vider le cache à chaque modification des injections
 */
// $builder->enableCompilation(__DIR__ . '/../cache');
$builder->addDefinitions(__DIR__ . '/../config/injections.php');
$container = $builder->build();

// Create App
$app = \DI\Bridge\Slim\Bridge::create($container);

// Initialise le gestionnaire de session
session_set_save_handler($container->get('SessionHandlerInterface'), true);
session_start();

// register middlewares
$middlewares = require __DIR__ . '/../config/middlewares.php';
$middlewares($app, $container);

// app error config
$errorMiddleware = $app->addErrorMiddleware($isDebugMode, true, true);
$errorMiddleware->setDefaultErrorHandler($container->get(ExceptionHandler::class));

$routeParser = $app->getRouteCollector()->getRouteParser();
$container->set(Slim\Interfaces\RouteParserInterface::class, $routeParser);

// API Endpoints
if ($isDebugMode) {
    $app->get(
        '/phpinfo',
        function () {
            phpinfo();
        }
    );
}

// Register routes
$routes = require __DIR__ . '/../config/routes.php';
$routes($app, $container);

// let's roll !
$app->run();
