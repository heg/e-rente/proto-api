<?php

use Dibs\Api\Data\DataManager;
use Dibs\Api\Exceptions\ExceptionHandler;
use Dibs\Api\Http\CorsMiddleware;
use Dibs\Api\Login\LoginManager;
use Dibs\Api\OpenAK\OpenAKManager;
use Dibs\Api\OpenPK\OAuthProviderFactory as OpenPKOAuthProviderFactory;
use Dibs\Api\OpenPK\OpenPKManager;
use Dibs\Api\OpenPV\OAuthProviderFactory as OpenPVOAuthProviderFactory;
use Dibs\Api\OpenPV\OpenPVManager;
use Dibs\Api\User\UserManager;
use GuzzleHttp\Client as GuzzleClient;
use Predis\Client as RedisClient;
use Predis\ClientInterface as RedisClientInterface;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;

// use Aptoma\Twig\Extension\MarkdownExtension;
// use Aptoma\Twig\Extension\MarkdownEngine\MichelfMarkdownEngine as MarkdownEngine;

return [
    /**
     * Configuration du gestionnaire de session pouvant être utilisé avec session_set_save_handler
     * (si on veut implémenter des sessions encore plus sécurisé, en redis ou en mysql, c'est là !)
     * Pour l'instant on utilise \SessionHandler qui est le gestionnaire de session par défaut
     */
    'SessionHandlerInterface'         => function (ContainerInterface $c) {
        return $c->get('\SessionHandler');
    },

    /**
     * Le gestionnaire des exceptions
     */
    ExceptionHandler::class           => DI\factory([ExceptionHandler::class, 'getHandler']),

    /**
     * Le middleware permettant de retourner les informations CORS dans les headers
     */
    CorsMiddleware::class             => function () {
        return new CorsMiddleware();
    },

    /**
     * Gestionnaire de connexion oAuth pour authentifié l'utilisateur courant
     * Permet de se connecter via un service oAuth pour SwissId, auth0, Google ?
     * Pour l'instant on utilise un Gestionnaire générique, mais au besoin on pourrait préciser le gestionnaire par exemple GoogleProvider ou FacebookProvider
     * Le loginManager attend un provider implémentant League\OAuth2\Client\Provider\AbstractProvider
     */
    LoginManager::class               => function (ContainerInterface $c) {
        $protocol = strpos(
            strtolower($_SERVER['INTERFACE_DOMAIN']),
            'https'
        ) === false ? 'http' : 'https';
        $redirectUri = ($_ENV['REDIRECT_DOMAIN'] ?? ($protocol . '://' . $_SERVER['HTTP_HOST'])) . '/login/connect';

        return new LoginManager(
            new \Dibs\Api\Login\OAuthProvider(
                [
                    'clientId'                => $_ENV['LOGIN_CLIENT_ID'],
                    'clientSecret'            => $_ENV['LOGIN_CLIENT_SECRET'],
                    'redirectUri'             => $redirectUri,
                    'logoutUri'               => $_ENV['LOGIN_LOGOUT_URL'],
                    'urlAuthorize'            => $_ENV['LOGIN_URL_AUTHORIZE'],
                    'urlAccessToken'          => $_ENV['LOGIN_URL_ACCESS_TOKEN'],
                    'urlResourceOwnerDetails' => $_ENV['LOGIN_URL_RESOURCE_OWNER_DETAILS'],
                ]
            )
        );
    },

    /**
     * Gestionnaire de connexion vers OpenAK
     */
    OpenAKManager::class              => function (ContainerInterface $c) {
        return new OpenAKManager(
            new GuzzleClient(
                [
                    GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::READ_TIMEOUT    => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::TIMEOUT         => $_ENV['TIMEOUT'],
                    'base_uri'                                 => $_ENV['OPENAK_API'],
                    GuzzleHttp\RequestOptions::SSL_KEY         => $_ENV['OPENAK_CERTS_KEY'],
                    GuzzleHttp\RequestOptions::CERT            => $_ENV['OPENAK_CERTS_CRT'],
                    GuzzleHttp\RequestOptions::AUTH            => [
                        $_ENV['OPENAK_CLIENT_ID'], $_ENV['OPENAK_CLIENT_SECRET'],
                    ],
                ]
            ),
            $c->get(UserManager::class),
        );
    },

    /**
     * Gestionnaire de connexion vers OpenPK
     * Permet de récupérer le registre des caisses de retaite
     * Permet d'interagir avec les différentes caisses de retraites
     */
    OpenPKManager::class              => function (ContainerInterface $c) {
        return new OpenPKManager(
            new GuzzleClient(
                [
                    GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::READ_TIMEOUT    => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::TIMEOUT         => $_ENV['TIMEOUT'],
                    'base_uri'                                 => $_ENV['OPENPK_REGISTRY_API'],
                ]
            ),
            $c->get(DataManager::class),
            $c->get(UserManager::class),
            $c->get(OpenPKOAuthProviderFactory::class)
        );
    },

    /**
     * Générateur d'oAuthProviders
     * Chaque caisse de retraite à un flux oauth spécifique
     */
    OpenPKOAuthProviderFactory::class => function () {
        $protocol = strpos(
            strtolower($_SERVER['INTERFACE_DOMAIN']),
            'https'
        ) === false ? 'http' : 'https';
        $redirectUri = ($_ENV['REDIRECT_DOMAIN'] ?? ($protocol . '://' . $_SERVER['HTTP_HOST']))
            . '/openpk/user/pension-funds/tokens';
        $redirectGrantUri = ($_ENV['REDIRECT_DOMAIN'] ?? ($protocol . '://' . $_SERVER['HTTP_HOST']))
            . '/openpk/user/pension-funds/grants';

        return new OpenPKOAuthProviderFactory(
            redirectUri: $redirectUri,
            redirectGrantUri: $redirectGrantUri,
            timeout: $_ENV['TIMEOUT']
        );
    },

    /**
     * Gestionnaire de connexion vers OpenPK
     * Permet de récupérer le registre des caisses de retaite
     * Permet d'interagir avec les différentes caisses de retraites
     */
    OpenPVManager::class              => function (ContainerInterface $c) {
        return new OpenPVManager(
            new GuzzleClient(
                [
                    GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::READ_TIMEOUT    => $_ENV['TIMEOUT'],
                    GuzzleHttp\RequestOptions::TIMEOUT         => $_ENV['TIMEOUT'],
                    'base_uri'                                 => $_ENV['OPENPV_REGISTRY_API'],
                ]
            ),
            $c->get(DataManager::class),
            $c->get(UserManager::class),
            $c->get(OpenPVOAuthProviderFactory::class)
        );
    },

    /**
     * Générateur d'oAuthProviders
     * Chaque caisse de retraite à un flux oauth spécifique
     */
    OpenPVOAuthProviderFactory::class => function () {
        $protocol = strpos(
            strtolower($_SERVER['INTERFACE_DOMAIN']),
            'https'
        ) === false ? 'http' : 'https';
        $redirectUri      = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/openpv/user/tokens';
        $redirectGrantUri = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/openpv/user/grants';

        return new OpenPVOAuthProviderFactory(
            $_ENV['OPENPV_CLIENT_ID'],
            $_ENV['OPENPV_CLIENT_SECRET'],
            $redirectUri,
            $redirectGrantUri,
            $_ENV['TIMEOUT']
        );
    },

    /**
     * Le client de gestionnaire de base de données
     */
    RedisClientInterface::class       => function () {
        return new RedisClient();
    },

    /**
     *  Injection de l'extension Twig pour les vues si Twig est utilisé dans le controlleur
     */
    Twig::class                       => DI\factory(
        function (/*MarkdownExtension $markdownExtension*/) {
            $twig = Twig::create(
                __DIR__ . '/../templates',
                [
                    'cache' => false, //  __DIR__ . '/../cache',
                ]
            );

            // $twig->addExtension($markdownExtension);

            return $twig;
        }
    )
    /*)->parameter('markdownExtension', function (ContainerInterface $c) {
    return new MarkdownExtension($c->get(MarkdownEngine::class));
    })*/,
];
