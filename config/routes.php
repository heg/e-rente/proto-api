<?php

declare(strict_types=1);

use Dibs\Api\Controllers\LoginController;
use Dibs\Api\Controllers\Open3KController;
use Dibs\Api\Controllers\OpenAKController;
use Dibs\Api\Controllers\OpenPKController;
use Dibs\Api\Controllers\OpenPVController;
use Dibs\Api\Controllers\OpenPVFakeImplentationController;
use Dibs\Api\Controllers\SynthesisController;
use Dibs\Api\Login\IsAuthentifiedMiddleware;
use DI\Container;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app, Container $container) {

    /**
     * Routes non authentifiées
     */
    $app->group(
        '',
        function (RouteCollectorProxy $group) {
            /**
             * Permet de se connecter
             */
            $group->get('/login/connect', [LoginController::class, 'token'])->setName('login');

            /**
             * Permet de se déconnecter
             */
            $group->get('/login/disconnect', [LoginController::class, 'disconnect']);

            /**
             * Redirection depuis l'API vers l'interface (notamment appelé après la déconnexion)
             */
            $group->get('/redirect', [LoginController::class, 'redirect']);

            /**
             * Récupère la liste des pensions auquelles on peut se connecter
             */
            $group->get('/login/pension-funds', [OpenPKController::class, 'getLoginRegistry'])->setName('login-pension-funds');

            /**
             * Récupère la liste des pensions auquelles on peut se connecter
             */
            $group->get('/login/pension-funds/connect', [LoginController::class, 'loginViaPensionFund'])->setName('login-pension-funds');

            /**
             * Permet d'accepter toutes les routes "options" automatiquement
             */
            $group->options('[/{params:.*}]', [LoginController::class, 'options']);
        }
    );

    /**
     * Routes authentifés via Cookie ou Authorization header
     */
    $app->group(
        '',
        function (RouteCollectorProxy $group) {
            /**
             * Récupère les informations de l'utilisateur connecté
             */
            $group->get('/me', [LoginController::class, 'me']);
            /**
             * Efface toutes les infos de l'utilisateur connecté
             */
            $group->get('/login/reset', [LoginController::class, 'reset']);

            /**
             * Les routes liées à la synthèse
             */
            $group->group(
                '/synthesis',
                function (RouteCollectorProxy $group) {
                    /**
                     * Retourne la synthèse de l'utilisateur
                     */
                    $group->get('/digest', [SynthesisController::class, 'digest']);
                }
            );

            /**
             * Les routes liées à OpenAK
             */
            $group->group(
                '/openak',
                function (RouteCollectorProxy $group) {
                    /**
                     * Tester la connexion à OpenAK
                     */
                    $group->get('/test-connection', [OpenAKController::class, 'test']);
                    /**
                     * Récupère les informations de la personne correspondant au navs
                     */
                    $group->get('/insureds/{navs}', [OpenAKController::class, 'navsInfo']);
                    /**
                     * Enregistre les informations 1er pilier de l'utilisateur
                     */
                    $group->post('/pension-calculator/info', [OpenAKController::class, 'postPensionCalculatorInfo']);
                    /**
                     * Récupère  les informations 1er pilier de l'utilisateur
                     */
                    $group->get('/pension-calculator/info', [OpenAKController::class, 'getPensionCalculatorInfo']);
                    /**
                     * Récupère les projections de pension pour l'utilisateur dont on a enregistré des informations
                     * 1er pillier
                     */
                    $group->get('/pension-calculator', [OpenAKController::class, 'retrievePensionCalculator']);
                }
            );

            /**
             * Les routes liées à OpenPK
             */
            $group->group(
                '/openpk',
                function (RouteCollectorProxy $group) {

                    /**
                     * Récupère le registre des caisses de retraites
                     */
                    $group->get('/pension-funds', [OpenPKController::class, 'getRegistry']);

                    /**
                     * Permet de séléctionner les caisses de pensions de l'utilisateur
                     * - nécessite un tableau d'ids dans le body
                     * {
                     * "ids": [<string>]
                     * }
                     */
                    $group->post('/user/pension-funds', [OpenPKController::class, 'setPensionFunds']);

                    /**
                     * Récupère les fonds de pension séléctionné par l'utilisateur
                     */
                    $group->get('/user/pension-funds', [OpenPKController::class, 'getPensionFunds']);

                    /**
                     * Récupère tous les tokens des différentes caisses de pension
                     */
                    $group->get('/user/pension-funds/tokens', [OpenPKController::class, 'getNextConsent']);

                    /**
                     * Vérifie tous les granting des fonds de pensions
                     */
                    $group->get('/user/pension-funds/grants', [OpenPKController::class, 'getNextGrant']);

                    /**
                     * Vérifie tous les granting des fonds de pensions
                     */
                    $group->get('/user/pension-funds/grants/ok', [OpenPKController::class, 'grantingIsOkay']);

                    /**
                     * Récupère les contrats des fonds de pension de l'utilisateur
                     */
                    $group->get('/user/pension-funds/policies', [OpenPKController::class, 'getPolicies']);
                }
            );

            /**
             * Les routes liées à OpenPV
             */
            $group->group(
                '/openpv',
                function (RouteCollectorProxy $group) {

                    /**
                     * Récupère le registre des caisses de retraites
                     */
                    $group->get('/entities', [OpenPVController::class, 'getRegistry']);

                    /**
                     * Permet de séléctionner les fonds de pensions de l'utilisateur
                     * - nécessite un tableau d'ids dans le body
                     * {
                     * "ids": [<string>]
                     * }
                     */
                    $group->post('/user/entities', [OpenPVController::class, 'setEntities']);

                    /**
                     * Récupère les fonds de pension séléctionnés par l'utilisateur
                     */
                    $group->get('/user/entities', [OpenPVController::class, 'getEntities']);

                    /**
                     * Récupère tous les tokens des différents fonds de pension
                     */
                    $group->get('/user/tokens', [OpenPVController::class, 'getNextConsent']);

                    /**
                     * Vérifie tous les granting des fonds de pensions
                     */
                    $group->get('/user/grants', [OpenPVController::class, 'getNextGrant']);

                    /**
                     * Vérifie tous les granting des fonds de pensions
                     */
                    $group->get('/user/grants/ok', [OpenPVController::class, 'grantingIsOkay']);

                    /**
                     * Récupère le tableau des identifiants de fond de pension
                     */
                    $group->get('/user/providents', [OpenPVController::class, 'getProvidents']);
                }
            );

            /**
             * Les routes liées à Open3K
             */
            $group->group(
                '/open3k',
                function (RouteCollectorProxy $group) {
                    /**
                     * Permet de récupérer les infos Open3k
                     * ie :
                     * {
                     * "solde": "100000",
                     * "amount": "6883",
                     * "rate": "0.15",
                     * "paydate": "2027-02-01",
                     * "payout_predication": null
                     * }
                     */
                    $group->get('/info', [Open3KController::class, 'getInfo']);
                    /**
                     * Permet d'enregistrer les infos Open3k
                     */
                    $group->post('/info', [Open3KController::class, 'postInfo']);
                }
            );
        }
    )->add($container->get(IsAuthentifiedMiddleware::class));

    /**
     * Toutes les routes liées à l'implémentation du faux serveur oAuth
     * pour l'exemple de fonctionnement OpenPV
     */
    $app->group(
        '/openpv-fake-implementation',
        function (RouteCollectorProxy $group) {
            $group->get('/registry', [OpenPVFakeImplentationController::class, 'getRegistry']);
            $group->get('/protocol/openid-connect/auth', [OpenPVFakeImplentationController::class, 'auth']);
            $group->post('/protocol/openid-connect/token', [OpenPVFakeImplentationController::class, 'token']);
            $group->get('/protocol/openid-connect/grant', [OpenPVFakeImplentationController::class, 'grant']);
            $group->get('/protocol/openid-connect/grant/ok', [OpenPVFakeImplentationController::class, 'grantIsOk']);
            $group->get('/users/me', [OpenPVFakeImplentationController::class, 'me']);
            $group->get('/providents/{id}', [OpenPVFakeImplentationController::class, 'providents']);
        }
    );
};
