<?php

declare(strict_types=1);

use Dibs\Api\Http\CorsMiddleware;
use Dibs\Api\Http\JsonBodyParserMiddleware;
use Slim\App;
use DI\Container;

return function (App $app, Container $container) {
// app CORS config
    $app->add($container->get(CorsMiddleware::class));
    $app->add($container->get(JsonBodyParserMiddleware::class));
};
