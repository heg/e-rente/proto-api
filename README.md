# Api du projet eRentes.ch

## pile technique

- Slim Framwork 4
- php-di 6
- Client Oauth: une implémentation de `League\OAuth2\Client\Provider\AbstractProvider`. Pour le moment nous utilisons `League\OAuth2\Client\Provider\GenericProvider`
- Http Client: `GuzzleHttp\Client`
- DB: une implémentation de `Predis\ClientInterface` => `Predis\Client`

## Installation

    cd ROOT_DIRECTORY
    cp .env.example .env
    composer install

Puis il faut remplir les informations manquantes dans le fichier .env

## lancement serveur de dev

    cd ROOT_DIRECTORY
    php -S 0.0.0.0:1338 -t public public/index.php

## Création d'une nouvelle version

    cd ROOT_DIRECTORY
    npm version --no-git-tag-version patch|minor|major

## Sessions

Pour l'instant, le gestionnaire de session est le gestionnaire par défaut de PHP, pour changer ça il suffit de changer l'injection de `SessionHandler` (dans `config/injections.php`) et d'utiliser un gestionnaire de session implémentant l'interface [SessionHandlerInterface](https://www.php.net/manual/fr/class.sessionhandlerinterface.php)

### Données stockées dans la session

- Les informations de l'access token pour l'authentification au service (SwissId, auth0, ...)

## Authentification

Les utilisateurs peuvent se connecter via des services d'authentification externe (auth0, SwissId ...), les informations nécessaires à la connection doivent être mise dans le fichier .env

## Concept de développement

Nous avons essayé de séparer au maximum les différents objets du code :

- le serveur se lance par le fichier [public/index.php](./public/index.php) qui va s'occuper d'intialiser la pile technique
- tous les composants sont configurés de façon dynamique par [un injecteur de dépendance](https://php-di.org/). La configuration complète de tous les éléments de l'appli se trouvde dans le fichier [config/injections.php](./config/injections.php). L'étude de ce fichier vous montrera que la plupart des objets se configurent via les variables d'environnement.
- pour finir, toutes les routes se trouvent dans le fichier [config/routes.php](./config/routes.php)

## Routes

La lecture du fichier [config/routes.php](./config/routes.php) vous donnera toutes les routes de l'application.